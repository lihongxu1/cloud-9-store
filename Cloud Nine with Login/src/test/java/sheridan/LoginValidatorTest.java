package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testReq1IsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses123" ) );
	}
	
	@Test
	public void testReq1IsInvalidLoginRegular( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "6ramses!!" ) );
	}
	
	@Test
	public void testReq1IsValidLoginRegularBIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "r6amses" ) );
	}
	
	@Test
	public void testReq1IsInvalidLoginRegularBOutIntFirst( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "7rmses" ) );
	}
	
	@Test
	public void testReq1IsInvalidLoginRegularBOutSpecialCharacter( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "r!mses" ) );
	}
	
	@Test
	public void testReq2IsValidLoginRegular( ) {
		assertTrue("Invalid login, not long enough" , LoginValidator.isValidLoginName( "ramses123fd" ) );
	}
	
	@Test
	public void testReq2IsInvalidLoginRegular( ) {
		assertFalse("Invalid login, not long enough" , LoginValidator.isValidLoginName( "ra" ) );
	}
	
	@Test
	public void testReq2IsValidLoginRegularBIn( ) {
		assertTrue("Invalid login, not long enough" , LoginValidator.isValidLoginName( "ram6es" ) );
	}
	
	@Test
	public void testReq2IsInvalidLoginRegularBOut( ) {
		assertFalse("Invalid login, not long enough" , LoginValidator.isValidLoginName( "rm6es" ) );
	}


}
