package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	private static final String regex = "^[a-zA-Z][A-Za-z0-9]{5,}$";

	public static boolean isValidLoginName(String loginName) {

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(loginName);

		if (matcher.matches()) {
			return true;
		}

		else
			return false;

	}
}
